## Running the project locally

There is a simple `Dockerfile` and `docker-compose.yml` included with the project. The login for the Django admin is `admin@example.com / admin`

    1) First install Docker if you haven't done so al ready.
    2) Open a new terminal
    3) Navigate to the project directory
    4) Run the project by typing "docker-compose run -p 8000:8000 parity" in the terminal.
    5) Navigate to "http://localhost:8000/api/docs/" to see which endpoints are available
    6) The Django admin is available on "http://localhost:8000/admin/"

## Running the tests

    1) First install Docker if you haven't done so al ready.
    2) Open a new terminal
    3) Navigate to the project directory
    4) Run the tests by typing "docker-compose run parity-tests" in the terminal.
    5) Tests should all pass

## Simple example of what the `houses` endpoint response would look like

This will be the json structure of a `House`, each item contains a `detail` field which can be used to interact with that specific object.

```
{
    "name": "43 Hanna",
    "detail": "http://localhost:8000/api/houses/1/",
    "furnace": {
        "state": "heat",
        "detail": "http://localhost:8000/api/furnaces/1/"
    },
    "rooms": [
        {
            "name": "Bedroom",
            "celsius": "12.0000",
            "fahrenheit": "53.6000",
            "detail": "http://localhost:8000/api/rooms/1/",
            "lights": [
                {
                    "name": "Ceiling",
                    "detail": "http://localhost:8000/api/lights/1/",
                    "state": "on"
                },
                {
                    "name": "Bedside Table Lamp",
                    "detail": "http://localhost:8000/api/lights/2/",
                    "state": "on"
                }
            ]
        },
        {
            "name": "Living Room",
            "celsius": "18.0000",
            "fahrenheit": "64.4000",
            "detail": "http://localhost:8000/api/rooms/2/",
            "lights": [
                {
                    "name": "Ceiling",
                    "detail": "http://localhost:8000/api/lights/3/",
                    "state": "off"
                }
            ]
        }
    ]
}
```
