# -*- coding: utf-8 -*-

from __future__ import division, print_function, unicode_literals

from django.apps import apps


def house_save_handler(sender, **kwargs):
    """
    Post save signal handler to create a `Furnace` when a new `House` is created.
    """
    created = kwargs.get('created', False)
    instance = kwargs.get('instance', None)
    if created and instance:
        apps.get_model('homes', 'furnace').objects.create(house=instance)


def furnace_save_handler(sender, **kwargs):
    """
    Post save signal handler to store `FurnaceStateHistory`.
    """
    instance = kwargs.get('instance', None)
    changed_data = instance.tracker.changed()
    if not kwargs.get('created', False) and changed_data and changed_data.get('state'):
        instance.furnacestatehistory_set.create(previous_state=changed_data['state'])


def light_save_handler(sender, **kwargs):
    """
    Post save signal handler to store `LightStateHistory`.
    """
    instance = kwargs.get('instance', None)
    changed_data = instance.tracker.changed()
    if not kwargs.get('created', False) and changed_data and changed_data.get('state'):
        instance.lightstatehistory_set.create(previous_state=changed_data['state'])


def room_save_handler(sender, **kwargs):
    """
    Post save signal handler to store `TemperatureHistory`.
    """
    instance = kwargs.get('instance', None)
    changed_data = instance.tracker.changed()
    if not kwargs.get('created', False) and changed_data and changed_data.get('celsius') is not None:
        instance.temperatures.create(celsius=changed_data['celsius'])
