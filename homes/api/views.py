# -*- coding: utf-8 -*-

from __future__ import division, print_function, unicode_literals

from rest_framework import mixins, status, viewsets
from rest_framework.decorators import detail_route
from rest_framework.response import Response

from homes.api.serializers import HouseSerializer, LightSerializer, RoomSerializer, FurnaceSerializer
from homes.models import House, Light, Room, Furnace


class UpdateDestroyViewSet(
        mixins.UpdateModelMixin, mixins.RetrieveModelMixin, mixins.DestroyModelMixin, viewsets.GenericViewSet):
    """
    ViewSet to not allow create.
    """


class HouseViewSet(viewsets.ModelViewSet):
    """
    ViewSet to create, update and retrieve houses.
    """
    serializer_class = HouseSerializer
    queryset = House.objects.all()

    @detail_route(methods=['post'], serializer_class=RoomSerializer)
    def add_room(self, request, *args, **kwargs):
        """
        Method to create a new room linked to an existing house.
        """
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            serializer.save(house=self.get_object())
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class RoomViewSet(UpdateDestroyViewSet):
    """
    ViewSet to update and remove existing rooms.
    """
    serializer_class = RoomSerializer
    queryset = Room.objects.all()

    @detail_route(methods=['post'], serializer_class=LightSerializer)
    def add_light(self, request, *args, **kwargs):
        """
        Method to create a new light linked to an existing room.
        """
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            serializer.save(room=self.get_object())
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class FurnaceViewSet(mixins.RetrieveModelMixin, mixins.UpdateModelMixin, viewsets.GenericViewSet):
    """
    ViewSet to update the Furnace state.
    """
    serializer_class = FurnaceSerializer
    queryset = Furnace.objects.all()


class LightViewSet(UpdateDestroyViewSet):
    """
    ViewSet to update and remove existing lights.
    """
    serializer_class = LightSerializer
    queryset = Light.objects.all()
