# -*- coding: utf-8 -*-

from __future__ import division, print_function, unicode_literals

import json

from rest_framework import status
from rest_framework.reverse import reverse

from django.test import TestCase

from homes.api.factories import HouseFactory, LightFactory, RoomFactory
from homes.models import Furnace, Light


class HomeAPITestCase(TestCase):

    def test_create_home(self):
        """
        Test that a new home can be created via the api.
        """
        response = self.client.post(reverse('house-list'), data={'name': '43 Hanna'})
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data, {
            'name': '43 Hanna',
            'rooms': [],
            'detail': response.data['detail'],
            'furnace': {
                'state': Furnace.STATE.off,
                'detail': response.data['furnace']['detail']
            }
        })

    def test_create_room(self):
        """
        Test that a new room can be created and linked to a home via the api.
        """
        house = HouseFactory()
        # Create a new room
        response = self.client.post(
            reverse('house-add-room', kwargs={'pk': house.pk}),
            data={'name': 'Bedroom', 'celsius': 0})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, {
            'lights': [],
            'detail': response.data['detail'],
            'celsius': '0.0000',
            'fahrenheit': '32.0000',
            'name': 'Bedroom'
        })

    def test_update_furnace_state(self):
        """
        Test that the furnace state can be updated.
        """
        house = HouseFactory()
        self.assertEqual(house.furnace.state, Furnace.STATE.off)
        # Update the furnace to `heat`
        furnace_response = self.client.patch(
            reverse('furnace-detail', kwargs={'pk': house.pk}),
            data=json.dumps({'state': 'heat'}),
            content_type='application/json'
        )
        self.assertEqual(furnace_response.status_code, status.HTTP_200_OK)
        self.assertEqual(furnace_response.data, {
            'state': Furnace.STATE.heat,
            'detail': furnace_response.data['detail']
        })
        # Check that the house response is correct after changing the furnace state
        response = self.client.get(reverse('house-detail', kwargs={'pk': house.pk}))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, {
            'name': '43 Hanna',
            'rooms': [],
            'detail': response.data['detail'],
            'furnace': {
                'state': Furnace.STATE.heat,
                'detail': response.data['furnace']['detail']
            }
        })

    def test_lights_create(self):
        """
        Test that the new lights can be created.
        """
        room = RoomFactory()
        # Create the light linked to a room
        response = self.client.post(reverse('room-add-light', kwargs={'pk': room.pk}), data={'name': 'Ceiling'})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, {
            'detail': response.data['detail'],
            'state': Light.STATE.off,
            'name': 'Ceiling'
        })
        # Check that the house response is correct after changing the furnace state
        response = self.client.get(reverse('house-detail', kwargs={'pk': room.house.pk}))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, {
            'detail':  response.data['detail'],
            'furnace': {
                'state': Furnace.STATE.off,
                'detail': response.data['furnace']['detail'],
            },
            'name': '43 Hanna',
            'rooms': [{
                'name': 'Bedroom',
                'detail': response.data['rooms'][0]['detail'],
                'celsius': '0.0000',
                'fahrenheit': '32.0000',
                'lights': [{
                    'name': 'Ceiling',
                    'detail': response.data['rooms'][0]['lights'][0]['detail'],
                    'state': Light.STATE.off
                }],
            }]
        })

    def test_room_temperature_update(self):
        """
        Test that the room temperatures can be updated.
        """
        room = RoomFactory()
        self.assertEqual(room.celsius, 0)
        self.assertEqual(room.get_fahrenheit(), 32.0)
        # Create the temperature of the room
        response = self.client.patch(
            reverse('room-detail', kwargs={'pk': room.pk}),
            data=json.dumps({'celsius': 5}),
            content_type='application/json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, {
            'fahrenheit': '41.0000',
            'lights': [],
            'celsius': '5.0000',
            'name': 'Bedroom',
            'detail': response.data['detail']
        })
        # Check that the house response is correct after changing room temperature
        response = self.client.get(reverse('house-detail', kwargs={'pk': room.house.pk}))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, {
            'detail':  response.data['detail'],
            'furnace': {
                'state': Furnace.STATE.off,
                'detail': response.data['furnace']['detail'],
            },
            'name': '43 Hanna',
            'rooms': [{
                'name': 'Bedroom',
                'detail': response.data['rooms'][0]['detail'],
                'celsius': '5.0000',
                'fahrenheit': '41.0000',
                'lights': [],
            }]
        })

    def test_light_state_update(self):
        """
        Test that the lights can be turned on.
        """
        light = LightFactory()
        # Create the light linked to a room
        response = self.client.get(reverse('light-detail', kwargs={'pk': light.pk}))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, {
            'detail': response.data['detail'],
            'state': Light.STATE.off,
            'name': 'Ceiling'
        })
        # Make a request to turn the light on
        light_response = self.client.patch(
            reverse('light-detail', kwargs={'pk': light.pk}), data=json.dumps({'state': Light.STATE.on}),
            content_type='application/json')
        self.assertEqual(light_response.status_code, status.HTTP_200_OK)
        self.assertEqual(light_response.data, {
            'detail': light_response.data['detail'],
            'state': Light.STATE.on,
            'name': 'Ceiling'
        })
        # Check that the house response is correct after turning a light on
        response = self.client.get(reverse('house-detail', kwargs={'pk': light.room.house.pk}))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, {
            'detail':  response.data['detail'],
            'furnace': {
                'state': Furnace.STATE.off,
                'detail': response.data['furnace']['detail'],
            },
            'name': '43 Hanna',
            'rooms': [{
                'name': 'Bedroom',
                'detail': response.data['rooms'][0]['detail'],
                'fahrenheit': '32.0000',
                'celsius': '0.0000',
                'lights': [{
                    'name': 'Ceiling',
                    'detail': response.data['rooms'][0]['lights'][0]['detail'],
                    'state': Light.STATE.on
                }],
            }]
        })
