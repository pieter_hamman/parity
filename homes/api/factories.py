# -*- coding: utf-8 -*-

from __future__ import absolute_import, division, print_function, unicode_literals

from factory import DjangoModelFactory, SubFactory

from homes.models import House, Light, Room


class HouseFactory(DjangoModelFactory):
    """
    Factory for the House model.
    """

    class Meta:
        model = House
        django_get_or_create = ('name', )

    name = '43 Hanna'


class RoomFactory(DjangoModelFactory):
    """
    Factory for the Room model.
    """

    class Meta:
        model = Room
        django_get_or_create = ('name', )

    name = 'Bedroom'
    house = SubFactory(HouseFactory)
    celsius = 0


class LightFactory(DjangoModelFactory):
    """
    Factory for the Light model.
    """

    class Meta:
        model = Light
        django_get_or_create = ('name', )

    name = 'Ceiling'
    room = SubFactory(RoomFactory)
    state = Light.STATE.off
