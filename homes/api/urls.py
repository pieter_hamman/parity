# -*- coding: utf-8 -*-

from __future__ import division, print_function, unicode_literals

from rest_framework.routers import SimpleRouter

from django.conf.urls import include, url

from homes.api import views

router = SimpleRouter()
router.register(r'furnaces', views.FurnaceViewSet)
router.register(r'houses', views.HouseViewSet)
router.register(r'rooms', views.RoomViewSet)
router.register(r'lights', views.LightViewSet)

urlpatterns = [
    url(r'^', include(router.urls))
]
