# -*- coding: utf-8 -*-

from __future__ import division, print_function, unicode_literals

from rest_framework import serializers

from homes.models import Furnace, House, Light, Room


class LightSerializer(serializers.ModelSerializer):
    detail = serializers.HyperlinkedIdentityField(view_name='light-detail')

    class Meta:
        model = Light
        fields = ('name', 'detail', 'state')


class RoomSerializer(serializers.ModelSerializer):
    detail = serializers.HyperlinkedIdentityField(view_name='room-detail')
    lights = LightSerializer(many=True, read_only=True)
    fahrenheit = serializers.DecimalField(source='get_fahrenheit', read_only=True, max_digits=7, decimal_places=4)

    class Meta:
        model = Room
        fields = ('name', 'celsius', 'lights', 'detail', 'fahrenheit')


class FurnaceSerializer(serializers.ModelSerializer):
    detail = serializers.HyperlinkedIdentityField(view_name='furnace-detail')

    class Meta:
        model = Furnace
        fields = ('state', 'detail')


class HouseSerializer(serializers.ModelSerializer):
    detail = serializers.HyperlinkedIdentityField(view_name='house-detail')
    rooms = RoomSerializer(many=True, read_only=True)
    furnace = FurnaceSerializer(read_only=True)

    class Meta:
        model = House
        fields = ('name', 'rooms', 'detail', 'furnace')
