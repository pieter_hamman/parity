# -*- coding: utf-8 -*-

from __future__ import division, print_function, unicode_literals

from django.test import TestCase

from homes.models import Furnace, FurnaceStateHistory, House, Light, LightStateHistory, Room, TemperatureHistory


class SignalTestCase(TestCase):

    def test_furnace_furnace_state_create(self):
        """
        Test that a `Furnace` and an initial `FurnaceState` is created when a new `House` is created.
        """
        # Check that there is no previous data.
        self.assertEqual(House.objects.all().count(), 0)
        self.assertEqual(Furnace.objects.all().count(), 0)
        self.assertEqual(FurnaceStateHistory.objects.all().count(), 0)
        # Create a new `House`
        house = House.objects.create(name='Test')
        # Check that the `Furnace` and `FurnaceState` exists.
        self.assertTrue(Furnace.objects.filter(house=house).exists())
        # Update the `Furnace.state` field.
        house.furnace.state = Furnace.STATE.fan
        house.furnace.save()
        # Check that the history is stored.
        self.assertTrue(
            FurnaceStateHistory.objects.filter(furnace__house=house, previous_state=Furnace.STATE.off).exists())

    def test_temperature_light_light_state_create(self):
        """
        Test that the initial `Temperature` is created for a new `Room` and that the initial `LightState` is created
        when a new `Light` is created.
        """
        # Check that there is no previous data.
        self.assertEqual(House.objects.all().count(), 0)
        self.assertEqual(Room.objects.all().count(), 0)
        self.assertEqual(TemperatureHistory.objects.all().count(), 0)
        self.assertEqual(Light.objects.all().count(), 0)
        self.assertEqual(LightStateHistory.objects.all().count(), 0)
        # Create a new `House`.
        house = House.objects.create(name='Test')
        # Create a new `Room`
        room = Room.objects.create(house=house, name='Bedroom', celsius=0)
        # Update the `Room.celsius` field.
        room.celsius = 10.0000
        room.save()
        # Check that the history is stored.
        self.assertTrue(TemperatureHistory.objects.filter(room=room).exists())
        # Create a `Light`
        light = Light.objects.create(room=room)
        # Turn on the light
        light.state = Light.STATE.on
        light.save()
        # Check that the `LightState` exists.
        self.assertTrue(
            LightStateHistory.objects.filter(light=light, light__room=room, previous_state=Light.STATE.off).exists())
