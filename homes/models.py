# -*- coding: utf-8 -*-

from __future__ import division, print_function, unicode_literals

from decimal import Decimal
from model_utils import FieldTracker
from model_utils.choices import Choices
from model_utils.models import TimeStampedModel

from django.db import models
from django.db.models.signals import post_save
from django.utils.encoding import python_2_unicode_compatible

from homes.signals import furnace_save_handler, house_save_handler, light_save_handler, room_save_handler


class NameMixin(models.Model):
    """
    Mixin to not have to add the `name` field to each model.
    """
    name = models.CharField(max_length=100)

    class Meta:
        abstract = True


@python_2_unicode_compatible
class House(NameMixin):
    """
    Model to store house data.
    """
    def __str__(self):
        return 'House {}'.format(self.name)


@python_2_unicode_compatible
class Room(NameMixin):
    """
    Model to store room data.
    """
    house = models.ForeignKey(House, related_name='rooms')
    celsius = models.DecimalField(max_digits=6, decimal_places=4)
    tracker = FieldTracker(fields=['celsius'])

    def __str__(self):
        return '{} in house {}'.format(self.name, self.house.name)

    def get_fahrenheit(self):
        """
        Helper method to calculate fahrenheit from celsius.
        """
        return round((self.celsius * Decimal(1.8) + Decimal(32)), 4)


@python_2_unicode_compatible
class Light(NameMixin):
    """
    Intermediary model to link a `Light` to a `Room`
    """
    room = models.ForeignKey(Room, related_name='lights')
    STATE = Choices(
        ('on', 'On'),
        ('off', 'Off'),
    )
    state = models.CharField(choices=STATE, default=STATE.off, max_length=3)
    tracker = FieldTracker(fields=['state'])

    def __str__(self):
        return '{} Light is {} in {}'.format(self.name, self.get_state_display(), self.room.name)


@python_2_unicode_compatible
class LightStateHistory(TimeStampedModel):
    """
    Model to store historic states of a `Light`.
    """
    light = models.ForeignKey(Light)
    previous_state = models.CharField(choices=Light.STATE, default=Light.STATE.off, max_length=3)

    def __str__(self):
        return 'Light state for room {}'.format(self.light.room.name)


@python_2_unicode_compatible
class Furnace(models.Model):
    """
    Intermediary model to link a `Furnace` to a `House`
    """
    house = models.OneToOneField(House)
    STATE = Choices(
        ('off', 'Off'),
        ('fan', 'Fan'),
        ('heat', 'Heat'),
    )
    state = models.CharField(choices=STATE, default=STATE.off, max_length=5)
    tracker = FieldTracker(fields=['state'])

    def __str__(self):
        return 'Furnace is {}'.format(self.house.name)


@python_2_unicode_compatible
class FurnaceStateHistory(TimeStampedModel):
    """
    Model to store historic states of a `Furnace`.
    """
    furnace = models.ForeignKey(Furnace)
    previous_state = models.CharField(choices=Furnace.STATE, default=Furnace.STATE.off, max_length=5)

    def __str__(self):
        return 'Furnace state history for house {}'.format(self.furnace.house.name)


@python_2_unicode_compatible
class TemperatureHistory(TimeStampedModel):
    """
    Model to store historic temperature data for a `Room`
    """
    room = models.ForeignKey(Room, related_name='temperatures')
    celsius = models.DecimalField(max_digits=6, decimal_places=4)

    def __str__(self):
        return 'Temperature for room {}'.format(self.room.name)

post_save.connect(house_save_handler, sender=House)
post_save.connect(room_save_handler, sender=Room)
post_save.connect(light_save_handler, sender=Light)
post_save.connect(furnace_save_handler, sender=Furnace)
