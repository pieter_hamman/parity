# -*- coding: utf-8 -*-

from __future__ import division, print_function, unicode_literals

from django.contrib import admin

from homes.models import House, Room, Light, Furnace, FurnaceStateHistory, TemperatureHistory, LightStateHistory


class FurnaceStateInline(admin.TabularInline):
    model = FurnaceStateHistory


class TemperatureInline(admin.TabularInline):
    model = TemperatureHistory


class LightStateInline(admin.TabularInline):
    model = LightStateHistory


class HouseAdmin(admin.ModelAdmin):
    list_display = ['name', ]


class RoomAdmin(admin.ModelAdmin):
    list_display = ['name', 'house', 'celsius', 'get_fahrenheit']
    inlines = [TemperatureInline]


class LightAdmin(admin.ModelAdmin):
    list_display = ['name', 'room', 'state']
    inlines = [LightStateInline, ]


class FurnaceAdmin(admin.ModelAdmin):
    list_display = ['house', 'state']
    inlines = [FurnaceStateInline]


admin.site.register(House, HouseAdmin)
admin.site.register(Room, RoomAdmin)
admin.site.register(Light, LightAdmin)
admin.site.register(Furnace, FurnaceAdmin)
admin.site.register(FurnaceStateHistory)
admin.site.register(TemperatureHistory)
admin.site.register(LightStateHistory)



