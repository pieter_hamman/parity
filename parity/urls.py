# -*- coding: utf-8 -*-

from __future__ import division, print_function, unicode_literals

from rest_framework_swagger.views import get_swagger_view

from django.conf.urls import include, url
from django.contrib import admin


schema_view = get_swagger_view(title='ParityGO API Docs')

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^accounts/', include('authtools.urls')),
    url(r'^api/', include('homes.api.urls')),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^api/docs/', schema_view),
]
